The executable for this project can be found in the Raytracer directory. It is named Raytracer.exe.

This can be executed by double clicking the executable in the file explorer.

This project has been developed for use on School of Computing machines, and has been tested as such.

To enable and disable antailiasing functionality, the tickbox in the user interface can be used, followed by clicking the 'Update' button. This will update the viewport with the resulting rendered image.

When antialiasing is enabled, there will be a delay before the viewport is updated due to calculation times. There is no indicator to show the viewer that calculations are being performed, so please be patient.

The 'Save' button in the user interface can be used to save a JPEG of the resulting rendered image in the parent directory.
