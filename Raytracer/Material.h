#ifndef MATERIAL_H
#define MATERIAL_H

#include "Utility.h"

class Material
{
public:
    Material();
    Material(Vector matColour, float ambientAlbedo, float diffuseAlbedo, float specularAlbedo, float specularExponent, float emission);
    ~Material() {}

    Vector matColour;
    float ambientAlbedo;
    float diffuseAlbedo;
    float specularAlbedo;
    float specularExponent;
    float emission;

};

#endif // MATERIAL_H
