#ifndef WORLD_H
#define WORLD_H

#include "Utility.h"
#include "Camera.h"
#include "Hittable.h"
#include "Ray.h"
#include "HitRecord.h"
#include "LightSource.h"

class World
{
public:
    World();
    Camera camera;
    std::vector<Hittable*> primitives;
    std::vector<LightSource*> lights;

    bool intersects(Ray &ray,HitRecord &rec);
};

#endif // WORLD_H
