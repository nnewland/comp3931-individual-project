#include "LightSource.h"

#include <iostream> //FOR TESTING PURPOSES ONLY, REMEMBER TO REMOVE LATER

LightSource::LightSource(Point position, float intensity)
{   //constructor
    this->position = position;
    this->intensity = intensity;
    lightColour = {255.,255.,255};
}   //constructor


// A method to determine whether the light source is the first object intersected in the scene.
bool LightSource::intersects(Ray &ray, HitRecord &rec) const
{   //intersects()
    // If no other geometry is intersected, the light source must be the first intersected object.
    if (rec.isHit == false) {
        return true;
    }
    // Otherwise, we must check whether the light source is closer i.e. has a smaller t value.
    else {
        float t = magnitude(position-ray.o);
        if (t < rec.t) {
            return true;
        }
    }
    return false;
}   //intersects()


// A stub to please the compiler.
Vector LightSource::getNormal(Point p) const {}   //getNormal()


// Retrieval function.
Point LightSource::getOrigin() const
{   //getOrigin()
    return position;
}   //getOrigin()


// Retrieval function.
Vector LightSource::getDirection(Point intersected)
{   //getDirection()
    Point o = getOrigin();
    return (normalise(o-intersected));
}   //getDirection()
