#ifndef SPHERE_H
#define SPHERE_H

#include "Material.h"
#include "Ray.h"
#include "Hittable.h"


class Sphere : public Hittable
{
public:
    Sphere();
    ~Sphere() {}
    Sphere(Point origin, double radius, Material material);

    virtual bool intersects(Ray &ray, HitRecord &rec) const override;
    virtual Vector getNormal(Point p) const override;
    virtual Point getOrigin() const override;
    virtual Material getMaterial() const override { return material; }

private:
    Point o;
    double r;
    Material material;

};

#endif // SPHERE_H
