#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QGLWidget>
#include <QObject>
#include <QDebug>
#include <QColor>
#include <QElapsedTimer>
#include "Raytracer.h"
#include "Ray.h"
#include "World.h"
#include "Sphere.h"
#include "Plane.h"
#include "Material.h"


class GlWidget: public QGLWidget
{
    Q_OBJECT

public:
    GlWidget(QWidget *parent);
    ~GlWidget();
    int widgetHeight, widgetWidth;
    void applyUpdate(bool anti);
    void applySave();
    World world;
    QElapsedTimer *timer;


protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();


private:
    QImage *image;
    bool antialiasing;
    void printImage();

};

#endif // GLWIDGET_H
