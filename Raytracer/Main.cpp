#include "MainWindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MainWindow *window = new MainWindow(NULL);
    window->showMaximized();

    app.exec();
    delete window;
    return 0;
}
