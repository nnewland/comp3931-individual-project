#include "Raytracer.h"


Raytracer::Raytracer(QImage *i, bool antialiasing, World world)
{   //constructor
    image = i;
    this->antialiasing = antialiasing;
    this->world = world;
}   //constructor

QImage Raytracer::render()
{   //render()
    Ray r;
    r.initRay(world.camera.at-world.camera.pos,world.camera.up);

        // Nested loops to iterate through each pixel in the image plane.
        for (int row = 0; row < image->height(); row++) {
            for (int col = 0; col <image->width(); col++) {
                colour = {0.,0.,0.};

                // If antialiasing enabled, 16 equally distributed rays should be cast through each pixel.
                if (antialiasing) {
                    for (int samplex = 1; samplex < 5; samplex++) {
                        for (int sampley = 1; sampley < 5; sampley++) {
                            r.calcRay(world.camera.pos, image->width(), image->height(),row, col, samplex, sampley, world.camera.focalLength, world.camera.fieldOfView);
                            trace(r);
                        }
                    }
                    // Average colour value taken from samples calculated.
                    colour /= 16;
                }
                // If antialiasing is disabled, only one ray must be cast through the centre of each ray.
                else {
                    r.calcRay(world.camera.pos,image->width(),image->height(),row,col,0,0,world.camera.focalLength, world.camera.fieldOfView);
                    trace(r);
                }

                // Final pixel colour value clamped to the range 0-255.
                colour = clamp(colour);
                QColor p(colour[0],colour[1],colour[2]);

                // Pixel in final image set to calculated colour value.
                image->setPixel(col,row,p.rgba());
            }
        }

    return *image;
}   //render()



//Testing intersection with all objects in the world, passing the closest to relevant colour calculations
Vector Raytracer::trace(Ray &ray)
{   //trace()
    HitRecord rec;
    Vector background(128.0,179.0,255.);

    world.intersects(ray, rec);

    // If no intersection is found, a predefined background colour is taken.
    if (rec.isHit == false) {
        colour += background;
    }
    // Otherwise, shade and lighting calculations should be performed at the point of intersection.
    else {
        shade(ray, rec);
    }
}   //trace()


void Raytracer::shade(Ray &ray, HitRecord &rec)
{   //shade()
    unsigned int i;
    HitRecord sRec;
    bool intersects;
    float intensity = 0;

    // Ambient contribution defined here, as it only needs to be added to the scene once.
    intensity += world.lights[0]->getIntensity()*rec.mat.ambientAlbedo;
    colour += intensity*rec.mat.matColour;

    for(i = 0; i < world.lights.size(); i++) {
        // For each light source, a shadow ray is cast from the surface to the light source.
        Ray s(jitterOrigin(rec.p,rec),world.lights[i]->getDirection(ray.pointAt(rec.t)));

        // Shadow ray is tested for intersection. If light is first point of intersection, intersects is true.
        world.intersects(s,sRec);
        intersects = world.lights[i]->intersects(s,sRec);

        if (intersects == true) {
            // Surface is in light, so control must be passed to lighting calculation function.
            intensity += world.lights[i]->getIntensity() * BlinnPhong(s.d,ray.d, rec.mat,rec, world.lights[i]);
        }
    }
}   //shade()


float Raytracer::BlinnPhong(Vector inDirection, Vector outDirection, Material material, HitRecord &rec, LightSource *light)
{   //BlinnPhong()

    inDirection = normalise(inDirection);
    outDirection = normalise(outDirection);

    //Diffuse contribution calculation
    float diffuse = material.diffuseAlbedo* angleCosine(inDirection, normalise(rec.n)) * light->getIntensity();
    colour += diffuse * rec.mat.matColour;


    //Specular contribution calculation
    Vector vh = (-inDirection + outDirection)/2.0f;
    float specular = pow(angleCosine(rec.n,vh),rec.mat.specularExponent) * light->getIntensity() * rec.mat.specularAlbedo;

    colour += max(specular,0) * light->getColour();

    return diffuse + specular;

}   //BlinnPhong()


// A function to move the origin point of a ray slightly in the direction of the surface normal to combat surface acne.
Point Raytracer::jitterOrigin(Point rOrigin, HitRecord &rec)
{   //jitterOrigin()
    float j = 0.0001;
    Point jittered = rOrigin + j*rec.n;
    return jittered;
}   //jitterOrigin()
