#include "Sphere.h"


Sphere::Sphere(Point origin, double radius, Material material)
{   //constructor
    o = origin;
    r = radius;
    this->material = material;
}   //constructor


bool Sphere::intersects(Ray &ray, HitRecord &rec) const
{   //intersects()
    //Make local copies of ray and sphere points and vectors
    Point rOrigin = ray.o;
    Point sOrigin = this->o;
    Vector u = rOrigin - sOrigin;
    Vector rDir = ray.d;

    //Calculate quadratic formula terms
    float a = glm::dot(rDir,rDir);
    float b = 2*glm::dot(u,rDir);
    float c = glm::dot(u,u) - (r*r);

    //Calculate discriminant
    float disc = b*b-4*a*c;

    //When discriminant is negative, there is no intersection
    if(disc < 0) {
        return false;
    }

    //Otherwise, calculate both possible solutions
    float t1 = (-b-sqrt(disc))/(2*a);
    float t2 = (-b+sqrt(disc))/(2*a);
    float t;

    if(t1 < rec.t_min || t1 > rec.t_max) {
        if(t2 < rec.t_min || t2 > rec.t_max) {
            return false;
        }
        else {
            t = t2;
        }
    }
    else {
        t = t1;
    }

    if(rec.isHit == true) {
        if(t < rec.t) {
            rec.t = t;
            rec.p = ray.pointAt(rec.t);
            rec.n = getNormal(rec.p);
            rec.mat = this->getMaterial();
        }
    }
    else {
        rec.t = t;
        rec.p = ray.pointAt(rec.t);
        rec.n = getNormal(rec.p);
        rec.mat = this->getMaterial();
    }

    rec.isHit = true;

    return true;

}   //intersects()


Vector Sphere::getNormal(Point p) const
{   //getNormal()
    //Calculates and returns the normal vector to the current sphere at point p
    Vector normal = p - o;

    return normal/=r;
}   //getNormal()

Point Sphere::getOrigin() const
{   //getOrigin
    return o;
}   //getOrigin
