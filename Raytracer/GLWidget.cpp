#include "GLWidget.h"
#include "MainWindow.h"
#include "Ray.h"
#include <QFile>

//FOR DEBUGGING
#include <iostream>


GlWidget::GlWidget(QWidget *parent)
    : QGLWidget(parent)
{   //constructor
    image = NULL;
    antialiasing = false;
}   //constructor


// Clean up scene objects to avoid memory leaks.
GlWidget::~GlWidget()
{   //destructor
    delete image;
    delete world.primitives[0];
    delete world.primitives[1];
    delete world.primitives[2];
    delete world.primitives[3];
    delete world.lights[0];
}   //destructor


// A method to update the viewport when antialiasing is enabled/disabled.
void GlWidget::applyUpdate(bool anti)
{   //applyUpdate()
    antialiasing = anti;
    glFlush();
    updateGL();
    printImage();
}   //applyUpdate()


// A method to save a jpeg of the resulting rendered image.
void GlWidget::applySave()
{   //applySave()
    image->save("render","jpeg",100);
}   //applySave()


// Initialises the environment and adds the defined geometry to the scene.
void GlWidget::initializeGL()
{   //initializeGL()
    glClearColor(0.,0.,0.,1.); //Set default background colour to white

    //material params: matColour, ambientAlbedo, diffuseAlbedo, specularAlbedo, specularExponent, emission
    Material m1(Vector(29,4,255),0.18,0.7,0.9,120,0.);
    Material m2(Vector(204,0,102),0.18,0.7,0.8,40,0.);
    Material m3(Vector(88,88,88),0.18,0.8,0.1,30,0.);
    Material m4(Vector(204,102,0),0.18,0.8,0.7,30,0.);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    world.primitives.push_back(new Sphere(Point(2,0.,-5),0.8,m2));
    world.primitives.push_back(new Sphere(Point(0,0.,-4.5),0.8,m4));
    world.primitives.push_back(new Sphere(Point(-4,0.,-5.5),2,m1));
    world.primitives.push_back(new Plane(Point(0.,-2.,0.),Vector(0.,0.99,0.),m3));
    world.lights.push_back(new LightSource(Point(5,5.,5.),1.));

}   //initializeGL()


// A method called whenever the viewport is resized. This defines a new QImage with the updated width and height.
void GlWidget::resizeGL(int w, int h)
{   //resizeGL()
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glViewport(0,0,w,h);
    glEnable(GL_LIGHTING);

    if(image != NULL) {
        delete image;
    }

    //Initialise image to new size of widget every time it resizes - responsive
    image = new QImage(w,h,QImage::Format_RGBA8888);
}   //resizeGL()


// Method called to 'paint' the viewport. This is where the Raytracer is initalised.
void GlWidget::paintGL()
{   //paintGL()
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();


    Raytracer tracer(image,antialiasing,world);
    *image = tracer.render();
    printImage();

}   //paintGL()


// A method to print the resulting rendered image to the screen.
void GlWidget::printImage()
{   //printImage()
    QImage *printedImage;
    printedImage = new QImage(image->width(), image->height(), QImage::Format_RGBA8888);
    *printedImage = QGLWidget::convertToGLFormat(*image);
    glDrawPixels(printedImage->width(), printedImage->height(), GL_RGBA, GL_UNSIGNED_BYTE, printedImage->bits());
    glFlush();
    delete printedImage;
}   //printImage()


