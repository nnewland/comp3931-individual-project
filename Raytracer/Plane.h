#ifndef PLANE_H
#define PLANE_H

#include <stdlib.h>
#include "Material.h"
#include "Utility.h"
#include "Ray.h"
#include "Hittable.h"


class Plane : public Hittable
{
public:
    Plane(Point origin, Vector normal, Material material);
    ~Plane() {}
    virtual bool intersects(Ray &ray, HitRecord &rec) const override;
    virtual Vector getNormal(Point p) const override;
    virtual Point getOrigin() const override;
    virtual Material getMaterial() const override { return material; }


private:
    Point o;
    Vector n;
    Material material;

};

#endif // PLANE_H
