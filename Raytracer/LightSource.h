#ifndef LIGHTSOURCE_H
#define LIGHTSOURCE_H

#include "Material.h"
#include "Hittable.h"
#include "Utility.h"


class LightSource : public Hittable
{
public:
    LightSource(Point position, float intensity);
    ~LightSource() {}

    virtual bool intersects(Ray &ray, HitRecord &rec) const override;
    virtual Vector getNormal(Point p) const override;
    virtual Point getOrigin() const override;
    virtual Material getMaterial() const override { return material; }

    float getIntensity() {return intensity;}
    Vector getDirection(Point intersected);
    Vector getColour() {return lightColour;}

private:
    Point position;
    float intensity;
    Vector lightColour;
    Vector ambientColour;
    Material material;
};

#endif // LIGHTSOURCE_H
