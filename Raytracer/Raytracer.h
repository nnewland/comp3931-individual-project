#ifndef RAYTRACER_H
#define RAYTRACER_H

#include <QObject>
#include <QImage>
#include <QColor>
#include "Hittable.h"
#include "Ray.h"
#include "Utility.h"
#include "Sphere.h"
#include "Plane.h"
#include "World.h"


class Raytracer : public QObject
{
    //mandatory for any object implementing signals or slots
    Q_OBJECT

public:
    Raytracer(QImage *i, bool antialiasing, World world);
    QImage render();
    World world;

protected:
    QColor pixCol;

private:
    Vector trace(Ray &ray);
    void traceShadow(Ray &ray, HitRecord &rec);
    Point jitterOrigin(Point rOrigin, HitRecord &rec);
    void shade(Ray &ray, HitRecord &rec);
    float BlinnPhong(Vector inDirection, Vector outDirection, Material material, HitRecord &rec, LightSource *light);

    QImage *image;
    bool antialiasing;
    Vector colour;
};

#endif // RAYTRACER_H
