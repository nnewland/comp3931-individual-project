#include "Plane.h"


Plane::Plane(Point origin, Vector normal, Material material)
{   //constructor
    o = origin;
    n = normal;
    this->material = material;
}   //constructor


bool Plane::intersects(Ray &ray, HitRecord &rec) const
{   //intersects()

    //Make local copies of ray data
    Point rOrigin = ray.o;
    Vector rDir = ray.d;
    float t;

    float denominator = glm::dot(this->n, rDir);
    if (fabs(denominator) > 0.001f) {
        Vector u = this->o - rOrigin;
        t = (glm::dot(u, this->n))/denominator;

        if (t >= 0) {
            // If a valid point of intersection is found, we must check if this is the first point of intersection for this ray.
            if (rec.isHit == true) {
                if (t < rec.t && t < rec.t_max && t > rec.t_min) {
                    rec.t = t;
                    rec.p = ray.pointAt(rec.t);
                    rec.n = getNormal(rec.p);
                    rec.mat = getMaterial();
                }
            }
            // If no other point of intersection is stored in the HitRecord, update it to include this intersection.
            else {
                rec.isHit = true;
                rec.t = t;
                rec.p = ray.pointAt(rec.t);
                rec.n = getNormal(rec.p);
                rec.mat = getMaterial();
            }
            return true;
        }
    }
    return false;
}   //intersects()


// Retrieval function.
Vector Plane::getNormal(Point p) const
{   //getNormal()
    return n;
}   //getNormal()


// Retrieval function.
Point Plane::getOrigin() const
{   //getOrigin()
    return o;
}   //getOrigin()
