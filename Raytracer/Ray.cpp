#include "Ray.h"


Ray::Ray(Point origin, Vector direction)
{   //constructor for secondary rays
    this->o = origin;
    this->d = direction;
}   //constructor for secondary rays


// A method which serves as a constructor for primary rays.
void Ray::initRay(Vector w, Vector up)
{   //initRay()
    this->w = normalise(-w);
    v = up -(glm::dot(up,this->w)*this->w);
    u = glm::cross(this->w,v);
}   //initRay()


void Ray::calcRay(Point origin, int widgetWidth, int widgetHeight, int row, int col, int samplex, int sampley, float focalLength, float fieldOfView)
{   //calcRay()

    o = origin;

    // Calculations to define the image plane, implementing the geometry defined in the report.
    float imageWidth = 2 * focalLength * tan(fieldOfView/2);
    float imageHeight = 2 * focalLength * tan(fieldOfView/2);
    float pixelWidth = imageWidth/widgetWidth;
    float pixelHeight = imageHeight/widgetHeight;
    float planeLeft = pixelWidth * -widgetWidth * 0.5;
    float planeTop = pixelHeight * widgetHeight * 0.5;

    float uPos, vPos;
    // If antialiasing is disabled, samplex and sampley will = 0.
    if (samplex == 0) {
        uPos = planeLeft + ((col + 0.5) * pixelWidth);
        vPos = planeTop - ((row + 0.5) * pixelHeight);
    }
    // If antialiasing is enabled, pixel coordinates should be calculated on a 4x4 basis.
    else {
        uPos = planeLeft + ((col + (samplex/4)) * pixelWidth);
        vPos = planeTop - ((row + (sampley/4)) * pixelHeight);
    }

    d = normalise(focalLength * -w + uPos*u + vPos * v);
}   //calcRay()


// A method to determine the point coordinates given a value, t, travelled down the ray.
Point Ray::pointAt(float t)
{   //pointAt()
    Vector td = t * d;

    return o + td;
}   //pointAt()
