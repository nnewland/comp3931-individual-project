#ifndef UTILITY_H
#define UTILITY_H

#include <GL/glu.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include <cmath>

using Vector = glm::vec3;
using Point = glm::vec3;

Vector normalise(Vector v);
float magnitude(Vector v);
float angleCosine(Vector a, Vector b);
float max(float a, float b);
float min(float a, float b);
Vector clamp(Vector v);


#endif // UTILITY_H
