#include "Material.h"

Material::Material()
{   //constructor
    matColour = Vector(0.,150.,100);
}   //constructor

Material::Material(Vector matColour, float ambientAlbedo, float diffuseAlbedo, float specularAlbedo, float specularExponent, float emission)
{   //constructor
    this->matColour = matColour;
    this->ambientAlbedo = ambientAlbedo;
    this->diffuseAlbedo = diffuseAlbedo;
    this->specularAlbedo = specularAlbedo;
    this->specularExponent = specularExponent;
    this->emission = emission;
}   //constructor
