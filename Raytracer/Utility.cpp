#include "Utility.h"

// Returns a unit vector corresponding to the given vector.
Vector normalise(Vector v)
{   //normalise()
    float mag = magnitude(v);

    return v /= mag;
}   //normalise()


// A function to calculate the magnitude of a given vector.
float magnitude(Vector v)
{   //magnitude()
    float mag = sqrt(pow(v[0],2) + pow(v[1],2) + pow(v[2],2));
    return mag;
}   //magnitude()


// A function to calculate the cosine of an angle between vectors a and b.
float angleCosine(Vector a, Vector b)
{   //angleCosine()
    float num = glm::dot(a,b);
    float denom = fabs(magnitude(a)) * fabs(magnitude(b));

    return num/denom;
}   //angleCosine()


// A function to return the maximum value of a and b.
float max(float a, float b)
{   //max()
    if (a > b) {
        return a;
    }
    return b;
}   //max()


// A function to return the minimum value of a and b.
float min(float a, float b)
{   //min()
    if (a < b) {
        return a;
    }
    return b;
}   //max()


// A function to clamp a vector to acceptable RGB values.
Vector clamp(Vector v)
{   //clamp()
    Vector w;
    w[0] = max(0.,min(v[0],255.));
    w[1] = max(0.,min(v[1],255.));
    w[2] = max(0.,min(v[2],255.));
    return w;
}   //clamp()

