#ifndef CAMERA_H
#define CAMERA_H

#include "Utility.h"

class Camera
{
public:
    Camera();
    ~Camera() {}
    Point getPos() { return pos; }
    Vector getDir() { return dir; }

    Point pos;
    Point at;
    Vector upDir;
    Vector dir;
    float focalLength;
    float fieldOfView;
    Vector forward;
    Vector left;
    Vector up;
};

#endif // CAMERA_H
