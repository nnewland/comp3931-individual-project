QT       += gui opengl

CONFIG += C++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Raytracer
TEMPLATE = app
INCLUDEPATH += . /opt/local/include

LIBS += -lGLU

SOURCES +=\
    Sphere.cpp \
    MainWindow.cpp \
    Main.cpp \
    Ray.cpp \
    GLWidget.cpp \
    Raytracer.cpp \
    Utility.cpp \
    Plane.cpp \
    Camera.cpp \
    HitRecord.cpp \
    World.cpp \
    LightSource.cpp \
    Material.cpp

HEADERS  += \
    Sphere.h \
    MainWindow.h \
    Ray.h \
    GLWidget.h \
    Raytracer.h \
    Utility.h \
    Plane.h \
    Camera.h \
    Hittable.h \
    HitRecord.h \
    World.h \
    LightSource.h \
    Material.h
