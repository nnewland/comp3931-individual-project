#ifndef HITRECORD_H
#define HITRECORD_H

#include "Utility.h"
#include "Material.h"

class HitRecord
{
public:
    HitRecord();
    ~HitRecord() {}

    bool isHit;
    Point p;
    float t;
    Vector n;
    float t_min;
    float t_max;
    Material mat;
};

#endif // HITRECORD_H
