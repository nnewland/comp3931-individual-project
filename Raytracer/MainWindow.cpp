#include "MainWindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
{   //constructor
    windowLayout = new QBoxLayout(QBoxLayout::LeftToRight,this);

    glWidget = new GlWidget(this);
    menuCont = menuWidget(this);
    windowLayout->addWidget(glWidget);
    windowLayout->addWidget(menuCont);
}   //constructor


QWidget *MainWindow::menuWidget(QWidget *parent)
{   //menuWidget()
    //This is where UI elements are defined.
    menuCont = new QWidget(parent);
    QBoxLayout *layout = new QBoxLayout(QBoxLayout::TopToBottom,menuCont);

    QLabel *title = new QLabel("Raytracer");
    title->setStyleSheet("font: bold 40px");
    title->setAlignment(Qt::AlignCenter);
    layout->addWidget(title);

    QWidget *row2 = new QWidget();
    QHBoxLayout *r2BoxL = new QHBoxLayout(row2);
    anti = new QCheckBox("Antialiasing?");
    QPushButton *update = new QPushButton("Update");
    connect(update, &QPushButton::released, this, &MainWindow::handleUpdateButton);
    QPushButton *save = new QPushButton("Save");
    connect(save, &QPushButton::released, this, &MainWindow::handleSaveButton);
    r2BoxL->addWidget(anti);
    r2BoxL->addWidget(update);
    r2BoxL->addWidget(save);
    layout->addWidget(row2);

    return menuCont;
}   //menuWidget()

void MainWindow::handleUpdateButton() {
    glWidget->applyUpdate(anti->checkState());
}


void MainWindow::handleSaveButton() {
    glWidget->applySave();
}


MainWindow::~MainWindow()
{   //destructor
    delete menuCont;
    delete glWidget;
    delete windowLayout;
}   //destructor
