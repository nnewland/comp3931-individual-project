#include "World.h"

World::World()
{   //constructor
    camera.pos = Point(0.,0,0.);
    camera.at = Point(0.,0.,-5.);
    camera.dir = normalise(camera.at - camera.pos);
    camera.up = Vector(0.,1.,0.);
    camera.focalLength = 100.;
    camera.fieldOfView = 90.;
}   //constructor


// A method to test for intersection between a ray and all primitives in a world.
bool World::intersects(Ray &ray, HitRecord &rec)
{   //intersects()
    bool doesIntersect = false;
    unsigned int i;

    for(i = 0; i < primitives.size(); i++) {
        doesIntersect = primitives[i]->intersects(ray, rec);
    }

    return doesIntersect;
}   //intersects()
