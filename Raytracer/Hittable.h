#ifndef HITTABLE_H
#define HITTABLE_H

#include "Ray.h"
#include "Utility.h"
#include "HitRecord.h"
#include "Material.h"


class Hittable
{
public:
    virtual ~Hittable() {}
    virtual bool intersects(Ray &ray, HitRecord &rec) const = 0;
    virtual Vector getNormal(Point p) const = 0;
    virtual Point getOrigin() const = 0;
    virtual Material getMaterial() const = 0;
};

#endif // HITTABLE_H
