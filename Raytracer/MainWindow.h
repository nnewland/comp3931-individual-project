#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QBoxLayout>
#include <QLabel>
#include <QCheckBox>
#include <QPushButton>
#include "GLWidget.h"


class MainWindow : public QWidget
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent);
    ~MainWindow();

    QBoxLayout *windowLayout;

    GlWidget *glWidget;

    QWidget *menuWidget(QWidget *parent);
    QWidget *menuCont;
    QCheckBox *anti;

public slots:
    void handleUpdateButton();
    void handleSaveButton();



};

#endif // MAINWINDOW_H
