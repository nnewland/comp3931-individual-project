#ifndef RAY_H
#define RAY_H

#include <QColor>
#include "Utility.h"


class Ray
{
    public:
        Ray() {}
        Ray(Point origin, Vector direction);
        void initRay(Vector w, Vector up);
        void calcRay(Point origin, int widgetWidth, int widgetHeight, int row, int col, int samplex, int sampley, float focalLength, float fieldOfView);

        Point origin() const { return o; }
        Vector direction() const { return d; }

        Point pointAt(float t);

        Point o;
        Vector d;

        float min_t;
        Vector u, v, w;

};

#endif // RAY_H
